# n-queens

A board is represented by a vector.
The position of a vector element is the column coordinate of a queen.
The value of the element is the row coordinate of a queen.

      Board [1 3 2 4]
      
      +---+---+---+---+
    1 | Q |   |   |   |
      +---+---+---+---+
    3 |   |   | Q |   |
      +---+---+---+---+
    2 |   | Q |   |   |
      +---+---+---+---+
    4 |   |   |   | Q |
      +---+---+---+---+


## Installation

Download from https://gitlab.com/matusfi/n-queens/builds/11921531/artifacts/file/target/uberjar/n-queens-0.1.0-SNAPSHOT-standalone.jar

## Usage

    $ java -jar n-queens-0.1.0-SNAPSHOT-standalone.jar <num-of-queens>

## Example

    $ java -jar n-queens-0.1.0-SNAPSHOT-standalone.jar 8
    92
    