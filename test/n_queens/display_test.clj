(ns n-queens.display-test
  (:require [clojure.test :refer :all]
            [n-queens.display :refer :all]))

(deftest test-transpose
  (testing "Transposing a board"
    (let [board      [3 2 4 5 1]
          exp        [5 2 1 3 4]
          transposed (transpose board)]
      (is (= exp transposed)))))
