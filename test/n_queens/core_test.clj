(ns n-queens.core-test
  (:require [clojure.test :refer :all]
            [n-queens.core :refer :all]))

(deftest test-solution?
  (testing "Is a certain board arrangement in a valid non-threatening queen position state"
    (let [valid   [5 3 1 7 2 8 6 4]
          invalid [1 2 3 4 5 6 7 8]]
      (is (true? (solution? valid)))
      (is (false? (solution? invalid))))))
