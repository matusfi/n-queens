(ns n-queens.display
  (:gen-class))

(defn- divider
  [size]
  (str "+"
       (apply str (repeat size "---+"))))

(defn- row
  [size position]
  (let [pre-queen-count  (dec position)
        post-queen-count (- size position)
        empty-pattern    "   |"
        queen-pattern    " Q |"]
    (str "|"
         (apply str (repeat pre-queen-count empty-pattern))
         queen-pattern
         (apply str (repeat post-queen-count empty-pattern)))))

;; TODO write it!
(defn transpose
  [board]
  board)

(defn show-board
  [board]
  (let [size          (count board)
        div           (divider size)
        col-positions (transpose board)]
    (do
      (println div)
      (doseq [pos col-positions]
        (println (row size pos))
        (println div)))))
