(ns n-queens.core
  (:gen-class)
  (:require [n-queens.display :as disp :refer [show-board]]
            [n-queens.generators :as gen]))

(set! *warn-on-reflection* true)

(defn reload []
  (use 'n-queens.core :reload-all))

(defn q-row
  "What row does the queen sit in, for the specified column?"
  [board col-pos]
  (nth board (dec col-pos)))

(defn horizontal-danger?
  "Check if there is any other queen in the same row.
  (Does the row number occur more than once in the vector?)"
  [board row]
  (> (count (filter #(= row %) board)) 1))

(defn next-col-danger?
  "Takes a queen's row position and another queen position with horizontal
  distance between them. Then checks if the two queens are on the same
  diagonal line."
  [row pair]
  (let [[dist next-row] pair]
    (or (= next-row (+ row dist))
        (= next-row (- row dist)))))

(defn distance-map
  [board]
  (zipmap (range 1 (inc (count board)))
          board))

(defn half-diagonal-danger?
  "Assumes the current queen is at the first column in the specified row.
  Checks if there are any other queens sitting in diagonal lines from the first."
  [half-board row]
  (if (empty? half-board)
    false
    (some #(next-col-danger? row %) (vec (distance-map half-board)))))

(defn diagonal-danger?
  [board col-pos]
  (let [row  (q-row board col-pos)
        left-part  (reverse (take (dec col-pos) board))
        right-part (drop col-pos board)]
    (or (half-diagonal-danger? left-part row)
        (half-diagonal-danger? right-part row))))

(defn danger?
  [board col-pos]
  (let [row (q-row board col-pos)]
    (or (horizontal-danger? board row)
        (diagonal-danger? board col-pos))))

(defn solution?
  [board]
  (let [queens-in-danger (for [col-pos (range 1 (inc (count board)))]
                           (danger? board col-pos))]
    (not-any? true? queens-in-danger)))

(defn count-solutions
  [boards]
  (count (filter solution? boards)))

(defn -main
  [& args]
  (let [size           (if (empty? args) 8 (read-string (first args)))
        solution-count (count-solutions (gen/gen-boards-brute-force size))]
    (println solution-count)))
