(ns n-queens.generators
  (:gen-class)
  (:require [clojure.math.combinatorics :as combo]))

;;
;; Board [ 1   3   2   4 ]
;;       +---+---+---+---+
;;       | Q |   |   |   |
;;       +---+---+---+---+
;;       |   |   | Q |   |
;;       +---+---+---+---+
;;       |   | Q |   |   |
;;       +---+---+---+---+
;;       |   |   |   | Q |
;;       +---+---+---+---+

(defn gen-base-board
  "Generates a board with given size with diagonal queens"
  [size]
  (vec (range 1 (inc size))))

(defn gen-boards-brute-force
  [size]
  (combo/permutations (gen-base-board size)))
